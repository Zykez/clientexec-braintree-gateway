# README #

### Overview ###

1. Requirements
2. Installation
2. Configure ClientExec
3. API Credentials
4. Support

### Requirements ###

* ClientExec 5.3.0+
* A Braintree Production & Sandbox Merchant Account

### Installation ###

1. Create the following directory: <CLientExecSiteRoot>/plugins/gateways/braintree>
2. Upload this archive to that directory
3. Extract the archive & set appropriate file permissions
4. Configure ClientExec

### Configure ClientExec ###

1. Navigate to: Settings -> Plugins -> Payment Processors
2. Select "Braintree" from the drop-down menu
3. Configure the plugin to your requirements. 

### API Credentials ###

Information on how to retrieve your API/Gateway credentials for use with this plugin can be found [here](https://articles.braintreepayments.com/control-panel/important-gateway-credentials).

### Support ###

Feature Requests, Issues, etc. can be accessed [here](https://bitbucket.org/Zykez/clientexec-braintree-gateway/issues).