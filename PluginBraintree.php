<?php
/*

============================== Braintree Gateway ===============================

Name: Braintree Processing Gateway Integration for ClientExec. 
Author: John A. Reed <john.reed@ifax.com> - iFAX Solutions, Inc.

License: See the LICENSE file included with this module. This license applies
exclusively to the code provided within this module. For ClientExec license
information, please visit their website at http://www.clientexec.com

Version: 0.1.1

===== Description =====

Braintree Credit Card Processing Gateway integration for ClientExec. Currently
only supports simple processing and refund methods.

The Braintree API can be found at: https://www.braintreepayments.com/docs/php

@package Plugins

*/
require_once 'modules/admin/models/GatewayPlugin.php';
require_once 'modules/billing/models/class.gateway.plugin.php';
require_once 'plugins/gateways/braintree/braintree-php/lib/Braintree.php';

class PluginBraintree extends GatewayPlugin {
	function getVariables() {
        $variables = array (
            lang("Plugin Name") => array (
                                "type"          =>"hidden",
                                "description"   =>lang("How CE sees this plugin ( not to be confused with the Signup Name )"),
                                "value"         =>"Braintree"
                                ),
            lang("Private Key") => array (
                                "type"          =>"text",
                                "description"   =>lang("Private Key from your Braintree Account."),
                                "value"         =>""
                                ),
            lang("Public Key") => array (
                                "type"          =>"text",
                                "description"   =>lang("Public Key from your Braintree Account."),
                                "value"         =>""
                                ),
            lang("Merchant ID") => array (
                                "type"          =>"text",
                                "description"   =>lang("Merchant ID from your Braintree Account."),
                                "value"         =>""
                                ),
            lang("Environment") => array (
                                "type"          =>"text",
                                "description"   =>lang("Production or Sandbox?"),
                                "value"         =>""
                                ),
            lang("Invoice After Signup") => array (
                                "type"          =>"yesno",
                                "description"   =>lang("Select YES if you want an invoice sent to the customer after signup is complete."),
                                "value"         =>"1"
                                ),
            lang("Signup Name") => array (
                                "type"          =>"text",
                                "description"   =>lang("Select the name to display in the signup process for this payment type. Example: eCheck or Credit Card."),
                                "value"         =>"Braintree"
                                ),
            lang("Dummy Plugin") => array (
                                "type"          =>"hidden",
                                "description"   =>lang("1 = Only used to specify a billing type for a customer. 0 = full fledged plugin requiring complete functions"),
                                "value"         =>"0"
                                ),
            lang("Accept CC Number") => array (
                                "type"          =>"hidden",
                                "description"   =>lang("Selecting YES allows the entering of CC numbers when using this plugin type. No will prevent entering of cc information"),
                                "value"         =>"1"
                                ),
            lang("Visa") => array (
                                "type"          =>"yesno",
                                "description"   =>lang("Select YES to allow Visa card acceptance with this plugin.  No will prevent this card type."),
                                "value"         =>"1"
                               ),
            lang("MasterCard") => array (
                                "type"          =>"yesno",
                                "description"   =>lang("Select YES to allow MasterCard acceptance with this plugin. No will prevent this card type."),
                                "value"         =>"1"
                               ),
            lang("AmericanExpress") => array (
                                "type"          =>"yesno",
                                "description"   =>lang("Select YES to allow American Express card acceptance with this plugin. No will prevent this card type."),
                                "value"         =>"1"
                               ),
            lang("Discover") => array (
                                "type"          =>"yesno",
                                "description"   =>lang("Select YES to allow Discover card acceptance with this plugin. No will prevent this card type."),
                                "value"         =>"1"
                               ),
            lang("DinersClub") => array (
                                "type"          =>"yesno",
                                "description"   =>lang("Select YES to allow Diners Club card acceptance with this plugin. No will prevent this card type."),
                                "value"         =>"0"
                               ),
            lang("Auto Payment") => array (
                                "type"          =>"hidden",
                                "description"   =>lang("No description"),
                                "value"         =>"1"
                                ),
            lang("30 Day Billing") => array (
                                "type"          =>"hidden",
                                "description"   =>lang("Select YES if you want ClientExec to treat monthly billing by 30 day intervals.  If you select NO then the same day will be used to determine intervals."),
                                "value"         =>"0"
                                ),
            lang("Check CVV2") => array (
                                "type"          =>"hidden",
                                "description"   =>lang("Select YES if you want to accept CVV2 for this plugin."),
                                "value"         =>"1"
                                )
        );
        return $variables;
	}

    /**
    * Charge a credit card
    * @param array $card_info An array of credit card info including:
    *     - first_name The first name on the card
    *     - last_name The last name on the card
    *     - card_number The card number
    *     - card_exp The card expiration date in yyyymm format
    *     - card_security_code The 3 or 4 digit security code of the card (if available)
    *     - type The credit card type
    *     - address1 The address 1 line of the card holder
    *     - address2 The address 2 line of the card holder
    *     - city The city of the card holder
    *     - state An array of state info including:
    *         - code The 2 or 3-character state code
    *         - name The local name of the state
    *     - country An array of country info including:
    *         - alpha2 The 2-character country code
    *         - alpha3 The 3-character country code
    *         - name The english name of the country
    *         - alt_name The local name of the country
    *     - zip The zip/postal code of the card holder
    * @param float $amount The amount to charge this card
    * @param array $invoice_amounts An array of invoices, each containing:
    *     - id The ID of the invoice being processed
    *     - amount The amount being processed for this invoice (which is included in $amount)
    * @return array An array of transaction data including:
    *     - status The status of the transaction (approved, declined, void, pending, error, refunded, returned)
    *     - reference_id The reference ID for gateway-only use with this transaction (optional)
    *     - transaction_id The ID returned by the remote gateway to identify this transaction
    *     - message The message to be displayed in the interface in addition to the standard message for this transaction status (optional)
    */
	
    function credit($params)
    {
        $params['refund'] = true;
        return $this->autopayment($params);
    }

    function singlePayment($params)
    {
        return $this->autopayment($params);
    }

    function autopayment($params)
    {
	return $this->loadApi();
	
	$currency = $params['currencytype'];
	$amount = $this->formatAmount($params['invoiceTotal']);

        // Attempt to charge the card
        $errors = array();
	$charge = array(
	        'amount'	=> $amount,
		'orderId'	=> $params['invoiceNumber'],
		'creditCard'	=> array(
			'number'          => $params['userCCNumber'],
			'expirationMonth' => $CCMo,
			'expirationYear'  => $CCYear,
			'cvv'             => $CCcvc,
			'cardholderName'  => $params["userFirstName"] . ' ' . $params["userLastName"]
			),
		'options' 	=> array(
			'submitForSettlement' => True
			)
	        );
		try {
			$response = Braintree_Transaction::sale($charge);
		}
        catch (Braintree_Exception_Authentication $exception) {
            $errors = array(401 => array(401 => "Braintree Exception: Authentication (Possible Environment/Credential Mismatch)"));
        }
        catch (Braintree_Exception_Authorization $exception) {
            $errors = array(403 => array(403 => "Braintree Exception: Authorization (Access Not Allowed) " . $exception->getMessage() ));
        }
        catch (Braintree_Exception_NotFound $exception) {
            $errors = array(404 => array(404 => "Braintree Exception: Not Found (Possibly Unable To Find Resource--Already removed?)"));
        }
        catch (Braintree_Exception_UpgradeRequired $exception) {
            $errors = array(426 => array(426 => "Braintree Exception: Upgrade Required"));
        }
        catch (Braintree_Exception_ServerError $exception) {
            $errors = array(500 => array(500 => "Braintree Exception: Server Error"));
        }
        catch (Braintree_Exception_DownForMaintenance $exception) {
            $errors = array(503 => array(503 => "Braintree Exception: Down For Maintenance"));
        }
        catch (Braintree_Exception_Unexpected $exception) {
            $errors = array('default' => array('default' => "Braintree Exception: General Error " . $exception->getMessage()));
        }

        // Set whether there was an error
        $status = "error";
        if ($response->success) {
            $status = "approved";
              if($charge->__get('object') == 'charge'){
                $cPlugin->setTransactionID($charge->__get('id'));
			$cPlugin->PaymentAccepted($response->amount, $response->status);
        } else if (is_object($response->transaction) && isset($response->transaction->status) ) {
            $message = $response->message;
            switch($response->transaction->status) {
                case "processor_declined":
                    $errors = array($response->transaction->status => array($response->transaction->processorResponseCode => $response->transaction->processorResponseText));
					$cPlugin->PaymentRejected($errors);
                    break;
                case "gateway_rejected":
                    $errors = array($response->transaction->status => array($response->transaction->status => $response->transaction->gatewayRejectionReason));
					$cPlugin->PaymentRejected($errors);
                    break;
            }
        } else if($response->errors) {
            $message = null;
            foreach (($response->errors->deepAll()) as $error) {
                $message .= $error->message . "<br>";
            }
        }


        // Set any errors
        if (!empty($errors))
            $this->Input->setErrors($errors);

        // Log the request
        $url = $this->base_url . "charges";
        $this->logRequest($url, $charge, $response);


        // Return formatted response
        $reference_id = substr($this->$params['userCCNumber'], -4);
        return array(
            'status'=>$status,
            'reference_id'=> (strlen($reference_id) == 0 ? null : $reference_id),
            'transaction_id'=> is_object($response->transaction) ? $response->transaction->id : null,
            'message'=> $this->$message
        );
    }
}
    function loadApi() {
        $cPlugin = new Plugin($params['invoiceNumber'], "braintree", $this->user);
        $cPlugin->setAmount($params['invoiceTotal']);

	$cPlugin->m_TransactionID = $params['invoiceNumber'];
	$cPlugin->m_Action = ($transType == 'credit')? 'refund' : $transType;
	$cPlugin->m_Last4 = mb_substr($params['userCCNumber'],-4);

        $CCMo   = mb_substr($params['userCCExp'], 0, 2);
        $CCYear = mb_substr($params['userCCExp'], 3);
        $CCcvc  = (is_numeric($params["userCCCVV2"]) ? $params["userCCCVV2"] : null);
        
        $environment = '$params["plugin_braintree_Environment"]';
        
        if ($environment == "production") {
        	$this->base_url = "https://api.braintreegateway.com/";
        	}else{
        	$this->base_url = "https://api.sandbox.braintreegateway.com/";
		}
		
        if (isset($params['refund']) && $params['refund']) {
		$isRefund = true;
		$cPlugin->setAction('refund');
		}else{
		$isRefund = false;
		$cPlugin->setAction('charge');
		}
		
	Braintree_Configuration::environment($params['plugin_braintree_Environment']);
	Braintree_Configuration::merchantId($params['plugin_braintree_Merchant ID']);
	Braintree_Configuration::publicKey($params['plugin_braintree_Public Key']);
	Braintree_Configuration::privateKey($params['plugin_braintree_Private Key']);
	CE_Lib::log(4, $params); 
    }
    /**
    * Log the request
    *
    * @param string $url The URL of the API request to log
    * @param array The input parameters sent to the gateway
    * @param array The response from the gateway
    */
    function logRequest($url, $params, $response) {
        // Define all fields to mask when logging
        $mask_fields = array(
            'number', // CC number
            'exp_month',
            'exp_year',
            'cvc'
        );

        $response = $this->objectToArray($response);

        // Determine success or failure for the response
        $success = false;
        if (!isset($response['error']))
            $success = true;

        // Log data sent to the gateway
        $this->log($url, serialize($this->$params, $mask_fields), "input", (isset($params['error']) ? false : true));

        // Log response from the gateway
        $this->log($url, serialize($this->$response, $mask_fields), "output", $success);
    }

    /**
     * Casts multi-dimensional objects to arrays
     *
     * @param mixed $object An object
     * @return array All objects cast to array
     */
    function objectToArray($object) {
        if (is_object($object))
            $object = get_object_vars($object);
        
        // Recurse over object to convert all object keys in $object to array
        if (is_array($object))
            return array_map(array($this, __FUNCTION__), $object);
        return $object;
    }


    /**
    * Convert amount from decimal value to integer representation of cents
    *
    * @param float $amount
    * @return int The amount in cents
    */
    function formatAmount($amount) {
        return round($amount, 2);
    }
}